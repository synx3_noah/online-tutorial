import express from 'express';
import bcrypt from 'bcrypt-nodejs';
import commonValidations from '../shared/validations/signup';
import { isEmpty } from 'lodash';
import assert from 'assert';

import User from '../models/user';
import mongo from '../mongodb';

let router = express.Router();

function validateInput(data, otherValidations) {
  let { errors } = otherValidations(data);

  mongo.db.collection('users')
    .findOne(
      {
        $or: [{ email: data.email }, { username: data.username }]
      },
      {},
      (err, user) => {
        // res.send(user);
        if(user.username === data.username) {
          errors.username = 'Username not available';
        }
        if(user.username === data.username) {
          errors.username = 'Email not available';
        }
        return {
          errors,
          isValid: isEmpty(errors)
        }
      }
    );

  // return User.query({
  //   where: { email: data.email},
  //   orWhere: { username: data.username }
  // }).fetch().then(user => {
  //   if(user) {
  //     if(user.get('username') === data.username) {
  //       errors.username = 'Username not available';
  //     }
  //     if(user.get('email') === data.email) {
  //       errors.email = 'Email not available';
  //     }
  //   }
  //
  //   return {
  //     errors,
  //     isValid: isEmpty(errors)
  //   }
  // })
}

router.post('/', (req, res) => {
  const { username, password, timezone, email, errors } = req.body;
  mongo.db.collection('users')
    .findOne(
      {
        $or: [{ email }, { username }]
      },
      {},
      (err, user) => {
        assert.equal(null, err);
        console.log(user);
        // console.log(req.body);
        if(user.username === req.body.username) {
          errors.username = 'Username not available';
        }
        if(user.email === req.body.email) {
          errors.email = 'Email not available';
        }
        if(isEmpty(errors)) {

          mongo.db.collection('users')
            .insertOne(
              req.params,
              (err, res) => {
                assert.equal(err, null);
                // console.log("Inserted a document into the restaurants collection.");
                // callback();
                res.json({ success: true});
            });

          // const password_digest = bcrypt.hashSync(password);
          // User.forge({
          //   username, timezone, email, password: password_digest
          // }, {hasTimestamps: true}).save()
          //   .then(user => res.json({ success: true }))
          //   .catch(err => res.status(500).json({ error: err }));
        } else {
          res.status(400).json(errors);
        }
      }
    );

  // validateInput(req.body, commonValidations).then(( { errors, isValid}) => {
  //   if(isValid) {
  //     const { username, password, timezone, email } = req.body;
  //     const password_digest = bcrypt.hashSync(password);
  //
  //     User.forge({
  //       username, timezone, email, password: password_digest
  //     }, {hasTimestamps: true}).save()
  //       .then(user => res.json({ success: true }))
  //       .catch(err => res.status(500).json({ error: err }));
  //   } else {
  //     res.status(400).json(errors);
  //   }
  // });
});

router.get('/', (req, res) => {
  mongo.db.collection('users')
    .findOne(
      {
        $or: [
          {
            email: 'bcook5@mapquest.com'
          },
          {
            username: 'bcook5'
          }
        ]
      },
      {},
      (err, user) => {
        res.send(user);
      }
    );
    // .toArray((err, users) => {
    //   res.send(users);
    // });
});

export default router;
